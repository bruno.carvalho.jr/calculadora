import React from 'react';
import './Calculator.css'

import Button from '../components/Button'
import Display from '../components/Display'

const initialState = {

    //valor do display
    displayValue: '0',
    //É para limpar o display?
    clearDisplay: false,
    //Quarda a operação a ser relizada
    operation: null,
    //Guarda duas posições de valores, um arrey com 2 valores
    //para realizar as operações
    values: [0,0],
    //Posição corrente do array de valores que estou trabalhando
    current: 0
}

export default class Calculator extends React.Component {

    //Cirando um clone do estado inicial e atribuindo a uma variavel
    state = {...initialState}

    constructor(props) {

        super(props)

        this.clearMemory = this.clearMemory.bind(this)
        this.setOperation = this.setOperation.bind(this)
        this.addDigit = this.addDigit.bind(this)
    }

    clearMemory() {

        //caso o clearMemory seja acionado ele volta o estado da calculadora (this) para o estado inicial
        this.setState({...initialState})
    }

    addDigit(n) {

        //evitando que 2 pontos seja adicionado no valor do display
        if(n === '.' && this.state.displayValue.includes('.')){
            return
        }

        //Vou limpar quando o valor for 0 ou o clearDisplay estiver true
        const clearDisplay = this.state.displayValue === '0'
            || this.state.clearDisplay

        //Valor corrente que vai ser vazio se precisar limpar o display e caso n]ao, o valor atual do display
        const currentValue = clearDisplay ? '' : this.state.displayValue

        //O novo valor do display recebe o valor corrente + o numero (ou ponto) digitado e concatena
        const displayValue = currentValue + n

        //Muda o estado do this. para o novo valor do display e seta o clearDisplay para false
        this.setState({displayValue, clearDisplay: false})
        
        //verifica se oque foi digitado foi diferente de ponto
        if(n !== '.') {
            //armazeno o valor corrente da pozição no array que estou trabalhando
            const i = this.state.current

            //guardando um novo valor com o valor do display sendo parseado para floate, para operações
            const newValue = parseFloat(displayValue)

            //Armazeno um clone dos atuais values 
            const values = [...this.state.values]

            //guardo o novo valor no array de valores
            values[i] = newValue

            //atribuo os novos valores a (this) calculadora
            this.setState({values})

            //para entender melhor veja o log dos valores ao clicar nos numeros
            console.log(values)
        }
    }

    setOperation(operation) {

        //Verifico se estou trabalhando na posição 0 dos valores para então passar a trabalhar na posição 1
        if(this.state.current === 0){
            //atribuo um novo estado no (this) com a atual operação, mudando o aposição para 1 e limpando o display
            this.setState({operation, current: 1, clearDisplay: true})
        } else {
            //verifico se o que ele digitou foi para saber o resultado final no display e guardo
            const result = operation === '='

            //Guardo a operação do meu this (operação corrente) atual que é a operação a ser
            //realizada e não a nova operação que chegou no setOperation
            const currentOperation = this.state.operation

            //então crio uma nova copia dos atuais valores e realizo a operação
            //armazenando o resultado na posição 0 do array e esvazio a posição 1
            const values = [...this.state.values]

            //TODO refactor
            //tratar isto depois, usando um switch case
            try{

                values[0] = eval(`${values[0]} ${currentOperation} ${values[1]}`)
                
            } catch(e) {

                values[0] = this.state.values[0]
            }

            values[1] = 0

            //seto o estado do this passando o valores na posição 0 para o display
            //a operação vai receber vazio se ele quis saber o resultado, caso contrario recebe a operação q digitou
            //se ele quer saber o resultado eu seto o index(current ou posição) para 0, caso contrario, para 1
            //caso ele não queira saber o resultado e não preciso limpar a tela imprimir 
            //e finalmente passamos os valores para o nosso this
            this.setState({
                displayValue: values[0],
                operation: result ? null : operation,
                current: result ? 0 : 1,
                clearDisplay: !result,
                values
            })

        }
    }


    render(){
        return(
            <div className="calculator">
                <Display value={this.state.displayValue} />
                <Button label="AC" click={this.clearMemory} triple/>
                <Button label="/" click={this.setOperation} operation/>
                <Button label="7" click={this.addDigit}/>
                <Button label="8" click={this.addDigit}/>
                <Button label="9" click={this.addDigit}/>
                <Button label="*" click={this.setOperation} operation/>
                <Button label="4" click={this.addDigit}/>
                <Button label="5" click={this.addDigit}/>
                <Button label="6" click={this.addDigit}/>
                <Button label="-" click={this.setOperation} operation/>
                <Button label="1" click={this.addDigit}/>
                <Button label="2" click={this.addDigit}/>
                <Button label="3" click={this.addDigit}/>
                <Button label="+" click={this.setOperation} operation/>
                <Button label="0" click={this.addDigit} double/>
                <Button label="." click={this.addDigit}/>
                <Button label="=" click={this.setOperation} operation/>
                
            </div>
        )
    }
}